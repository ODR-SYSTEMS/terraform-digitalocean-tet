#!/usr/bin/env bash
THEHOME="$(sudo su dbuser-backup -c '$HOME')"

sudo docker exec mysql /usr/bin/mysqldump -u dbuser_sa --password=newpassword5 ejs > /home/dbuser-backup/terraform-digitalocean-base/db-backup/ejs-db-backup-`date +'%Y%m%d%H%M%S'`.sql && sleep 1 &
BACK_PID=$!
wait $BACK_PID
sudo chown -R dbuser-backup:dbuser-backup /home/dbuser-backup/terraform-digitalocean-base/db-backup
echo "Backup complete"

BASE="/home/dbuser-backup/terraform-digitalocean-base/"
cd $BASE/db-backup
git add .
git commit -m "db backup `date +'%Y%m%d%H%M%S'`"
git push origin master
