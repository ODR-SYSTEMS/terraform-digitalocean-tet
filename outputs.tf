output "ip_address" {
  value = digitalocean_droplet.bastiondb2.ipv4_address
  description = "The public IP address of your Droplet application."
}
